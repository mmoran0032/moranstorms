# Access

Attempting to connect to the Hub via USB from my Linux computer have
failed. Directly talking to the device at `/dev/bus/usb/001/007` gave
access granted, as well as the python route:

```python
import usb.core

hub = usb.core.find(bDeviceClass=2)
hub.set_configuration()
# USBError: [Errno 13] Access denied (insufficient permissions)
```

There likely is a lock with the Windows application to the Hub that is
required for the Hub to accept the connection. When plugging it in, it
does go through the regular start-up sequence, but that may be based
simply on the availability of power (similar to other USB devices).
