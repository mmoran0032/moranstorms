import sys

from version import __version__ as __hub_version__

content = """
from mindstorms import MSHub

__version__ = "0.1.1-20220221"

def hello() -> None:
    print("hello from moranstorms {}".format(__version__))

def start_test_suite(hub: MSHub, name: str) -> None:
    print("TESTING {}...".format(name.upper()))
    hub.right_button.wait_until_pressed()

def test_complete(hub: MSHub, name: str) -> None:
    print("{} complete".format(name))
    hub.speaker.beep(60, 0.1)
    hub.right_button.wait_until_pressed()
"""

# run uploading process
print("Hub version:", __hub_version__)
# # persist the module on the Hub
# open("projects/__init__.py", "w").close()
f = open("mindstorms/custom.py", "w")
f.write(content)
f.close()
print("uploaded moranstorms")
sys.exit()
