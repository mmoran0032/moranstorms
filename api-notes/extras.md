# Extras

These are the things in `control` and `operator` that could be useful for more complex logic.

## Operators

These are just functional forms of regular comparisons. Since we don't have `functools` importable, we'll have to use this if we need comparisons passed into a function, like `wait_until()`.

## Wait

```
wait_until(get_value_function, operator_function=<function equal_to>, target_value=True)
Waits until the condition is true before continuing with the program.
Parameters
get_value_function
Type : callable function
Values : A function that returns the current value to be compared to the target value.
Default : no default value
operator_function
Type : callable function
Values : A function that compares two arguments. The first argument will be the result of get_value_function(), and the second argument will be target_value. The function will compare both values and return the result.
Default : no default value
target_value
Type : any type
Values : Any object that can be compared by operator_function.
Default : no default value
Errors
TypeError
get_value_function or operator_function is not callable or operator_function does not compare two arguments.
```

Example:

```python
from mindstorms import ColorSensor
from mindstorms.control import wait_until
from mindstorms.operator import equal_to

color_sensor = ColorSensor('A')

# Wait for the Color Sensor to detect "red."
wait_until(color_sensor.get_color, equal_to, 'red')
```

Example:

```python
from mindstorms import ColorSensor, Motor
from mindstorms.control import wait_until

color_sensor = ColorSensor('A')
motor = Motor('B')

def red_or_position():
    return color_sensor.get_color() == 'red' or motor.get_position() > 90

wait_until(red_or_position)
```

```
wait_for_seconds(seconds)
Waits for a specified number of seconds before continuing the program.
Parameters
seconds
The time to wait, specified in seconds.
Type : float (decimal value)
Values : any value
Default : no default value
Errors
TypeError
seconds is not a number.
ValueError
seconds is not at least 0.
```

## Timer

OK, we can reset the timer. This is good news!

```
reset()
Sets the Timer to "0."

now()
Retrieves the "right now" time of the Timer.
Returns
The current time, specified in seconds.
Type : Integer (a positive or negative whole number, including 0)
Values : A value greather than 0
```

Note that `now()` is from when the timer initialized or reset, and that the resolution is 1 sec.
