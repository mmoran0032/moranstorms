# Imports

By default, the MINDSTORMS package space is imported by default:

```python
from mindstorms import MSHub, Motor, MotorPair, ColorSensor, DistanceSensor, App
from mindstorms.control import wait_for_seconds, wait_until, Timer
from mindstorms.operator import greater_than, greater_than_or_equal_to, less_than, less_than_or_equal_to, equal_to, not_equal_to
import math
```

Some standard library packages can be imported:

```python
import array
import builtins
import cmath
import ctypes
import errno
import gc
import hashlib
import heapq
import io
import json
import math
import os
import random
import re
import select
import struct
import sys
import time
```

Creating and importing your own packages is more difficult. I figured out the below. Note that, since rename fails with `OSError` if the file exists, you can't use this to update the deployed package. But this is a very good start. I answered this question on [Stack Overflow](https://stackoverflow.com/questions/64449448/how-to-import-from-custom-python-modules-on-new-lego-mindstorms-robot-inventor):


1. Create a new python project with this content:
   ```python
   import os

   os.chdir("projects")
   open("__init__.py", "w").close()

   filename = "{}.py".format(__name__.split("/")[-1])
   new_filename = "mystorms.py"
   os.rename(filename, new_filename)

   # begin actual stuff you want to import
   def test_import() -> None:
       print("running code from {} that was imported".format(__name__))
   ```

2. Send that project to your Hub and run it. Note that you'll have those files that persist on your Hub, and I have no idea if there are memory constraints that you'll need to worry about.

3. Create a new python project with the following:
   ```python
   import os

   import projects.mystorms as ms

   print(os.uname())
   ms.test_import()
   ```

   You should see the output "running code from projects.mystorms that was imported" after the `uname` output.

The above should work. Note that if you need to *update* the file, you'll have to adjust the module code to delete the original if it exists, but that's a minor change. I have not done anything beyond the above, but this *could* lead to a way to get missing stdlib stuff into Mindstorms as well.
