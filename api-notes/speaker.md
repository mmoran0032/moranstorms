# Speaker

The Hub has a built-in speaker that can play sounds and individual beeps. The beeps are controlled through the following API:

```
def beep(self, note: int = 60, seconds: float = 0.2) -> None:
    """ play the MIDI note for the desired duration

    MIDI notes are specified as integers, where #60 is Middle C. A note
    reference can be found at http://computermusicresource.com/midikeys.html.
    Not all MIDI notes are supported. Note number must be in the range
    [44-123].

    This operation blocks until `seconds` has passed.
    """
    ...

def start_beep(self, note: int = 60) -> None:
    """ start the MIDI note

    See MIDI notes above. Note plays indefinitely until `stop()` is
    called or another beep method is called.

    This operation is not blocking.
    """
    ...

def stop(self) -> None:
    """ stop any playing beep"""
    ...

get_volume()
Retrieves the value of the speaker volume.
This only retrieves the volume of the Hub, not the programming app.
Returns
The current volume.
Type : integer (a positive or negative whole number, including 0)
Values
:
0 to 100%

set_volume(volume)
Sets the speaker volume.
If the assigned volume is out of range, the nearest volume (i.e., 0 or 100) will be used instead. This only sets the volume of the Hub, not the programming app.
Parameters
volume
The new volume percentage.
Type : integer (a positive or negative whole number, including 0)
Values : 0 to 100%
Default : 100%
```

The methods above can be combined into songs (likely combined with `wait_for_seconds()` for delays).
