# Buttons

*This needs to be updated*

Available buttons:

- `hub.left_button`: arrow on side of large button
- `hub.right_button`: arrow on side of large button

Do we get access to the big button, or Bluetooth? Likely not, since those are primary firmware
buttons that might not allow for user interaction. It's possible we could get it...

```
wait_until_pressed()
Waits until the button is pressed. Blocking.

wait_until_released()
Waits until the button is released. Blocking.

was_pressed()
Tests to see whether the button has been pressed since the last time this method called.
Once this method returns "true," the button must be released and pressed again before it will return "true" again.
Returns
True if the button was pressed, otherwise false
Type : Boolean
Values : True or False

is_pressed()
Tests whether the button is pressed.
Returns
True if the button is pressed, otherwise false
Type : Boolean
Values : True or False
```

Raw notes:

```
print("TESTING BUTTON PRESS...")
# while not hub.right_button.is_pressed():  # this did not break...likely based on checks
# there's some history at play here...
print(
    "left: {} ({})".format(
        hub.left_button.is_pressed(),
        hub.left_button.was_pressed()
    ),
    "right: {} ({})".format(
        hub.right_button.is_pressed(),
        hub.right_button.was_pressed()
    )
)
while not hub.right_button.was_pressed():
    hub.left_button.wait_until_pressed()  # blocking?
    hub.speaker.start_beep()
    hub.left_button.wait_until_released()
    hub.speaker.stop()
print("BUTTON TEST COMPLETE")
# result: after pressing right, the loop is stuck at wait_until_pressed(), which is blocking
# you need to press and release left to exit the loop
# is there a way around it?
```
