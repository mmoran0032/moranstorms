# API code notes

For our exploration of the Mindstorms functionality, there are two main
ways to accomplish this task:

1. write code that controls robotos
1. write code that inspects the code that you use to control robots

This directory contains the latter. The code is useful for determining
what methods are available and learning how to inspect Python code in
general. Each file roughly corresponds to a single object or subpackage
within Mindstorms. In general, the structure is as follows:

```python
# import useful python, micropython, and Mindstorms packages
import sys
import os

from mindstorms import ObjectName

# create the object we care about
obj = ObjectName()

# inspect the actual *.mpy code file and the created object
filename = "directory/objectname.mpy"
with open(filename, "r") as f:
    for line in f:
        print(line)

print("== introspection ==")
print(obj.__dict__)
object_methods = [
    _name for _name in dir(obj)
    if callable(getattr(obj, _name))
]
print(object_methods)
print(dir(obj))

# test some of the exposed methods
obj.do_something("Teleport", 50)

# close out our run with audible feedback
hub.speaker.beep()
sys.exit(0)
```
