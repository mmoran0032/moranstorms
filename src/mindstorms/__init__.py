# estimated code from compiled .mpy files

from _api.large_technic_hub import *

MSHub = LargeTechnicHub  # _api.large_technic_hub.LargeTechnicHub


# better user-friendliness code?
class MSHub:
    """
    the Mindstorms brain

    The MSHub ("Hub") is the embedded system that runs all Mindstorms
    programs and allows up to six peripherals (motors, sensors, etc.) to
    be connected and controlled. The Hub can be connected to through a
    wired USB connection or via Bluetooth.

    Attributes
    ----------
    left_button : Button
    right_button : Button
    """

    def __init__(self) -> None:
        ...


class Motor:
    def __init__(self) -> None:
        ...


class MotorPair:
    def __init__(self) -> None:
        ...


class ColorSensor:
    def __init__(self) -> None:
        ...


class DistanceSensor:
    def __init__(self) -> None:
        ...


class App:
    def __init__(self) -> None:
        ...
