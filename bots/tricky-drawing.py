"""
drawing-robot: Python port of the default program
"""

import os

from mindstorms import DistanceSensor, Motor, MotorPair, MSHub

print(os.uname())

driving_speed = 15


def init_robot():
    hub = MSHub()
    motor_travelpair = MotorPair("B", "A")  # reverse order, get different output
    # for B+A, based on current driving configuration, forward is opposite the distance sensor
    # for A+B, based on current driving configuration, forward is in the same direction as the distance sensor
    motor_travelpair.set_default_speed(driving_speed)
    sensor_distance = DistanceSensor("D")
    motor_aux = Motor("C")
    motor_aux.run_to_position(165, direction="shortest path", speed=75)
    print("robot initialized")
    return hub, motor_travelpair, sensor_distance, motor_aux


def draw_square(motor_travelpair: MotorPair, motor_pen: Motor):
    for _ in range(4):
        motor_pen.run_for_degrees(90)
        motor_travelpair.move(10, unit="cm")
        motor_pen.run_for_degrees(-90)
        motor_travelpair.move(170, unit="degrees")
        motor_travelpair.move(184, unit="degrees", steering=-100)
        motor_travelpair.move(10, unit="cm", speed=-driving_speed)


hub, motor_travelpair, sensor_distance, motor_aux = init_robot()
draw_square(motor_travelpair, motor_aux)
hub.speaker.beep(60, 0.25)
