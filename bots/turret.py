"""
turret

Required:
- Hub
- Motor
- DistanceSensor

Create base turret with large baseplate, motor, and turntable. Buttons control the
speed in increments, up to the maximum speed in either direction.
"""

from mindstorms import DistanceSensor, Motor, MSHub
from mindstorms.control import wait_for_seconds

hub = MSHub()
motor = Motor("B")
sensor_distance = DistanceSensor("D")

hub.speaker.beep(60, 0.1)
_ = hub.right_button.was_pressed()

starting_speed = 0
increment = 10
motor.start(speed=starting_speed)
current_distance = 200

while current_distance > 10:
    current_speed = motor.get_speed()
    if hub.right_button.was_pressed():
        hub.speaker.beep(60, 0.1)
        new_speed = current_speed + increment
        motor.start(new_speed)
    elif hub.left_button.was_pressed():
        hub.speaker.beep(60, 0.1)
        new_speed = current_speed - increment
        motor.start(new_speed)
    else:
        new_speed = current_speed
    current_distance = sensor_distance.get_distance_cm()
    if not current_distance:
        current_distance = 200
    print(current_speed, new_speed, current_distance)
    wait_for_seconds(0.1)

hub.speaker.beep(60, 0.1)
motor.stop()
